package cn.lis.mango.admin.controller;

import cn.lis.mango.admin.model.SysDict;
import cn.lis.mango.admin.service.SysDictService;
import cn.lis.mango.core.http.HttpResult;
import cn.lis.mango.core.page.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("dict")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;
    @PreAuthorize("hasAuthority('sys:dict:add') AND hasAuthority('sys:dict:edit')")
    @PostMapping(value = "/save")
    public HttpResult save(SysDict record) {
        return HttpResult.ok(sysDictService.save(record));
    }
    @PreAuthorize("hasAuthority('sys:dict:delete')")
    @PostMapping(value = "/delete")
    public HttpResult delete( List<SysDict> records) {
        return HttpResult.ok(sysDictService.delete(records));
    }
    @PreAuthorize("hasAuthority('sys:dict:view')")
    @PostMapping(value = "/findPage")
    public HttpResult findPage( PageRequest pageRequest) {
        return HttpResult.ok(sysDictService.findPage(pageRequest));
    }
    @PreAuthorize("hasAuthority('sys:dict:view')")
    @GetMapping(value = "/findByLable")
    public HttpResult findByLable(String lable) {
        return HttpResult.ok(sysDictService.findByLable(lable));
    }
}
