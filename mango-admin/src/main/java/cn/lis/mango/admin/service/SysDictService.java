package cn.lis.mango.admin.service;

import cn.lis.mango.admin.model.SysDict;
import cn.lis.mango.core.service.CurdService;

import java.util.List;

/**
 * 字典管理
 */
public interface SysDictService extends CurdService<SysDict> {
    /**
     * 根据名称查询
     * @param lable
     * @return
     */
    List<SysDict> findByLable(String lable);
}
