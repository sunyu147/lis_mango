package cn.lis.mango.admin.service;

import cn.lis.mango.admin.model.SysUser;
import cn.lis.mango.admin.model.SysUserRole;
import cn.lis.mango.core.page.PageRequest;
import cn.lis.mango.core.service.CurdService;

import java.io.File;
import java.util.List;
import java.util.Set;

public interface SysUserService extends CurdService<SysUser> {

    /**
     * 生成用户信息excel文件
     * @param pageRequest
     * @return
     */
    File createUserExcelFile(PageRequest pageRequest);

    SysUser findByName(String username);

    /**
     * 查找用户的菜单权限标识集合
     * @param userName
     * @return
     */
    Set<String> findPermissions(String userName);

    /**
     * 查找用户的角色集合
     * @param
     * @return
     */
    List<SysUserRole> findUserRoles(Long userId);
}
