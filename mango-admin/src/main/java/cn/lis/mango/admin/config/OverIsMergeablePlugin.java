package cn.lis.mango.admin.config;

import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import java.lang.reflect.Field;
import java.util.List;

public class OverIsMergeablePlugin extends PluginAdapter {
    @Override
    public boolean validate(List<String> list) {
        return true;
    }

    @Override
    public boolean sqlMapGenerated(GeneratedXmlFile sqlMap, IntrospectedTable introspectedTable) {
        try {
            Field filed = sqlMap.getClass().getDeclaredField("isMergeable");
            filed.setAccessible(true);
            filed.setBoolean(sqlMap,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
