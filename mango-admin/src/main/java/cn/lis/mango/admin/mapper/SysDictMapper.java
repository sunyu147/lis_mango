package cn.lis.mango.admin.mapper;

import cn.lis.mango.admin.model.SysDict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface SysDictMapper {
    /**
     * 分页查询
     * @return
     */
    List<SysDict> findPage();

    /**
     * 根据标签名称查询
     * @param label
     * @return
     */
    List<SysDict> findByLable(@Param("label") String label);

    /**
     * 根据名称分页查询
     * @param label
     * @return
     */
    List<SysDict> findPageByLabel(@Param(value = "label") String label);

    int insertSelective(SysDict record);

    int updateByPrimaryKeySelective(SysDict record);

    int deleteByPrimaryKey(Long id);

    SysDict selectByPrimaryKey(Long id);
}
