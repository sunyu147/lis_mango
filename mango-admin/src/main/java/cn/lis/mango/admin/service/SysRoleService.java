package cn.lis.mango.admin.service;

import java.util.List;

import cn.lis.mango.admin.model.SysMenu;
import cn.lis.mango.admin.model.SysRole;
import cn.lis.mango.admin.model.SysRoleMenu;
import cn.lis.mango.core.service.CurdService;
/**
 * 角色管理
 * @author Louis
 * @date Jan 13, 2019
 */
public interface SysRoleService extends CurdService<SysRole> {

	/**
	 * 查询全部
	 * @return
	 */
	List<SysRole> findAll();

	/**
	 * 查询角色菜单集合
	 * @return
	 */
	List<SysMenu> findRoleMenus(Long roleId);

	/**
	 * 保存角色菜单
	 * @param records
	 * @return
	 */
	int saveRoleMenus(List<SysRoleMenu> records);

	/**
	 * 根据名称查询
	 * @param name
	 * @return
	 */
	List<SysRole> findByName(String name);

}
