package cn.lis.mango.admin.mapper;

import cn.lis.mango.admin.model.SysDept;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;
import java.util.List;
@Mapper
@Resource
public interface SysDeptMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysDept record);

    int insertSelective(SysDept record);

    SysDept selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysDept record);

    int updateByPrimaryKey(SysDept record);
    
    List<SysDept> findPage();
    
    List<SysDept> findAll();
}